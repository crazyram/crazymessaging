﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Helpers;

namespace CrazyRam.Core.MessageBus
{
    public struct Channel<T>
    {
        internal T[] Array;

        internal int Tail;

        private int _expansion;

        private const int DefaultCapacity = 1;

        private const int DefaultExpand = 5;

        public bool HasNext
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => Tail > 0;
        }

        private Channel(T[] array, int tail)
        {
            Array = array;
            Tail = tail;
            _expansion = DefaultExpand;
        }

        public static Channel<T> New(int capacity = DefaultCapacity, int expand = DefaultExpand)
        {
            return new Channel<T>(new T[capacity], 0)
            {
                _expansion = expand
            };
        }

        public (T[] Data, int Count) FullDrain()
        {
            return (Array, Tail - 1);
        }

        public Channel<T> Flush()
        {
            System.Array.Clear(Array, 0, Array.Length);
            Tail = 0;
            return this;
        }

        public static Channel<T> operator +(Channel<T> channel, T value)
        {
            ArrayHelper.EnsureCapacity(ref channel.Array, channel.Tail + 1, channel._expansion);
            channel.Array[channel.Tail] = value;
            channel.Tail++;
            return channel;
        }
    }
}
