﻿namespace CrazyRam.Core.MessageBus
{
    public interface IRefQueueHandler<T>
    {
        // ReSharper disable once PureAttributeOnVoidMethod
        [System.Diagnostics.Contracts.Pure]
        void Process(in T data);
    }
}
