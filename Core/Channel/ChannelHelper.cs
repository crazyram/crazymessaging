﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.MessageBus
{
    public static class ChannelHelper
    {
        public static Option<T> Drain<T>(this ref Channel<T> channel) where T : struct
        {
            return channel.HasNext ? channel.Array[--channel.Tail].AsOption() : Option<T>.None;
        }

        /// <summary>
        /// Drain and clear object reference from source array
        /// </summary>
        /// <param name="channel"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Option<T> DrainSafe<T>(this ref Channel<T> channel) where T : class
        {
            if (channel.HasNext)
            {
                var result = channel.Array[--channel.Tail].AsOption();
                channel.Array[channel.Tail] = null;
                return result;
            }

            return Option<T>.None;
        }

        public static ref readonly T DrainRef<T>(this ref Channel<T> channel) where T : struct
        {
            return ref channel.Array[--channel.Tail];
        }

        // Example usage
        // int readId = 0;
        // while (TestCommutator<TestCall>.Default.CanDrainQueue(readId))
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CanDrainQueue<T>(in this Channel<T> channel, int id)
        {
            return id < channel.Tail;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T DrainQueueRaw<T>(this ref Channel<T> channel, ref int id) where T : struct
        {
            return channel.Array[id++];
        }

        // Example usage
        // int readId = 0;
        // Option<TestCall> call;
        // while ((call = TestCommutator<TestCall>.Default.DrainQueue(ref readId)).HasValue)
        public static Option<T> DrainQueue<T>(this ref Channel<T> channel, ref int id) where T : struct
        {
            if (CanDrainQueue(in channel, id))
                return channel.Array[id++].AsOption();
            ResetCounter(ref channel);
            return Option<T>.None;
        }

        public static Option<T> DrainQueueSafe<T>(this ref Channel<T> channel, ref int id) where T : class
        {
            if (CanDrainQueue(in channel, id))
            {
                var result = channel.Array[id++].AsOption();
                channel.Array[id] = null;
                return result;
            }

            ResetCounter(ref channel);
            return Option<T>.None;
        }

        // int readId = 0;
        // ref readonly var call = ref TestCommutator<TestCall>.Default.DrainQueueRef(ref readId);
        public static ref readonly T DrainQueueRef<T>(this ref Channel<T> channel, ref int id) where T : struct
        {
            return ref channel.Array[id++];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ResetCounter<T>(this ref Channel<T> channel)
        {
            channel.Tail = 0;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Handle<TData, THandler>(this ref Channel<TData> channel, in THandler handler)
            where THandler : struct, IRefQueueHandler<TData>
            where TData : struct
        {
            if (!channel.HasNext)
                return;
            int readId = 0;
            while (channel.CanDrainQueue(readId))
            {
                ref readonly var message = ref channel.DrainQueueRef(ref readId);
                handler.Process(in message);
            }

            channel.ResetCounter();
        }
    }
}
