﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.MessageBus
{
    public static class ClientPointerDispatcher<T>
    {
        public static PointerDispatcher<T> Default = PointerDispatcher<T>.New();
    }

    public static class ClientPointerDispatcherHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void RaisePointerEvent<T>(this ref T @event) where T : struct
        {
            ClientPointerDispatcher<T>.Default.Raise(ref @event);
        }
    }
}
