﻿namespace CrazyRam.Core.MessageBus
{
	public interface IPointerMessageReceiver<T>
	{
		void OnRefMessage(ref T data);
	}
}
