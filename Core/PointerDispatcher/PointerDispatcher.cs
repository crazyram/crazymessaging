﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Collections;

namespace CrazyRam.Core.MessageBus
{
    public struct PointerDispatcher<T>
    {
        public static PointerDispatcher<T> Default = New();

        public static PointerDispatcher<T> New()
        {
            return new PointerDispatcher<T>()
            {
                _subscribers = new CrazyList<IPointerMessageReceiver<T>>()
            };
        }

        private CrazyList<IPointerMessageReceiver<T>> _subscribers;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Subscribe(IPointerMessageReceiver<T> subscriber)
        {
            _subscribers.Add(subscriber);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Unsubscribe(IPointerMessageReceiver<T> subscriber)
        {
            _subscribers.Remove(subscriber);
        }

        public void Raise(ref T data)
        {
            if (_subscribers == null) return;
            for (int i = 0; i < _subscribers.Count; i++)
                _subscribers[i]?.OnRefMessage(ref data);
        }

        public void Clear()
        {
            _subscribers?.Clear();
        }

        public static PointerDispatcher<T> operator +(PointerDispatcher<T> source,
            IPointerMessageReceiver<T> subscriber)
        {
            source.Subscribe(subscriber);
            return source;
        }

        public static PointerDispatcher<T> operator -(PointerDispatcher<T> source,
            IPointerMessageReceiver<T> subscriber)
        {
            source.Unsubscribe(subscriber);
            return source;
        }
    }
}
