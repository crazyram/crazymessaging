﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Collections;

namespace CrazyRam.Core.MessageBus
{
    public struct Dispatcher<T>
    {
        private readonly CrazyList<IMessageReceiver<T>> _subscribers;

        private Dispatcher(CrazyList<IMessageReceiver<T>> subscribers)
        {
            _subscribers = subscribers;
        }

        public static Dispatcher<T> New()
        {
            return new Dispatcher<T>(new CrazyList<IMessageReceiver<T>>());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Subscribe(IMessageReceiver<T> subscriber)
        {
            _subscribers.Add(subscriber);
#if CRAZY_DEBUG_DISPATCHER
            UnityEngine.Debug.Log($"{typeof(T)} {subscriber} was subscribed {_subscribers.Count}");
#endif
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Unsubscribe(IMessageReceiver<T> subscriber)
        {
            _subscribers.Remove(subscriber);
#if CRAZY_DEBUG_DISPATCHER
            UnityEngine.Debug.Log($"{typeof(T)} {subscriber} was unsubscribed {_subscribers.Count}");
#endif
        }

        public void Raise(in T data)
        {
            if (_subscribers == null)
                return;
#if CRAZY_DEBUG_DISPATCHER
            UnityEngine.Debug.Log($"{_subscribers.Count} {data}");
            for (int i = 0; i < _subscribers.Count; i++)
                UnityEngine.Debug.Log($"{_subscribers[i]}");
            UnityEngine.Debug.Log("===============");
#endif
            for (int i = 0; i < _subscribers.Count; i++)
                _subscribers[i]?.OnMessage(in data);
        }

        public void Clear()
        {
            _subscribers?.Clear();
        }

        public static Dispatcher<T> operator +(Dispatcher<T> source, IMessageReceiver<T> subscriber)
        {
            source.Subscribe(subscriber);
            return source;
        }

        public static Dispatcher<T> operator -(Dispatcher<T> source, IMessageReceiver<T> subscriber)
        {
            source.Unsubscribe(subscriber);
            return source;
        }
    }
}
