﻿namespace CrazyRam.Core.MessageBus
{
	public interface IMessageReceiver<T>
	{
		void OnMessage(in T data);
	}
}
