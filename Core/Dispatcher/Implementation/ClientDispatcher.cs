﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.MessageBus
{
    public static class ClientDispatcher<T>
    {
        public static Dispatcher<T> Default = Dispatcher<T>.New();
    }

    public static class ClientDispatcherHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void RaiseEvent<T>(ref this T @event) where T : struct
        {
            ClientDispatcher<T>.Default.Raise(in @event);
        }
    }
}
