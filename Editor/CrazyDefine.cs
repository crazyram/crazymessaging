﻿using CrazyRam.Editor;
using UnityEditor;

namespace CrazyRam.Core.MessageBus
{
    [InitializeOnLoad]
    public sealed class CrazyDefine
    {
        private const string Define = "CRAZY_MESSAGE_BUS_INCLUDED";

        static CrazyDefine()
        {
            CrazyEngineDefineSetter.SetDefine(Define);
        }
    }
}
