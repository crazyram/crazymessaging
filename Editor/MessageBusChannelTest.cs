﻿using System.Collections.Generic;
using System.Linq;
using CrazyRam.Core;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.MessageBus;
using NUnit.Framework;

namespace CrazyRam.Editor
{
    public class MessageBusChannelTest
    {
        private struct TestCall
        {
            public readonly int Id;

            public TestCall(int id)
            {
                Id = id;
            }
        }

        private class Producer
        {
            public void Engage(int id)
            {
                TestCommutator<TestCall>.Default += new TestCall(id);
            }
        }

        private static class TestCommutator<T>
        {
            public static Channel<T> Default = Channel<T>.New();

            public static void Prepare()
            {
                Default = Channel<T>.New();
            }
        }

        private class Processor
        {
            public void Process(IEnumerator<int> scheme)
            {
                if (!TestCommutator<TestCall>.Default.HasNext)
                {
                    if (scheme != null && scheme.MoveNext())
                        Assert.Fail();
                    return;
                }

                scheme.MoveNext();
                Option<TestCall> call;
                while ((call = TestCommutator<TestCall>.Default.Drain()).HasValue)
                {
                    Assert.AreEqual(scheme.Current, call.Value.Id);
                    scheme.MoveNext();
                }
            }
        }

        [Test]
        public void MessageBusChannelTestSimplePush()
        {
            TestCommutator<TestCall>.Prepare();

            var producer = new Producer();

            producer.Engage(42);
            producer.Engage(12);
            producer.Engage(31);

            var call = TestCommutator<TestCall>.Default.Drain();
            Assert.AreEqual(31, call.Value.Id);

            call = TestCommutator<TestCall>.Default.Drain();
            Assert.AreEqual(12, call.Value.Id);

            call = TestCommutator<TestCall>.Default.Drain();
            Assert.AreEqual(42, call.Value.Id);

            call = TestCommutator<TestCall>.Default.Drain();
            Assert.AreEqual(Option<TestCall>.None, call);

            producer.Engage(92);
            call = TestCommutator<TestCall>.Default.Drain();
            Assert.AreEqual(92, call.Value.Id);
        }

        [Test]
        public void MessageBusChannelTestPush()
        {
            TestCommutator<TestCall>.Prepare();

            int bounds = UnityEngine.Random.Range(10, 20);
            var producer = new Producer();
            var processor = new Processor();
            var sequence = Enumerable.Range(0, bounds).ToList();
            sequence.ForEach(producer.Engage);

            processor.Process(Enumerable.Reverse(sequence).GetEnumerator());
        }

        [Test]
        public void MessageBusChannelTestDrain()
        {
            TestCommutator<TestCall>.Prepare();

            int bounds = UnityEngine.Random.Range(10, 20);
            int cycles = UnityEngine.Random.Range(2, 5);

            var producer = new Producer();
            var processor = new Processor();
            var sequence = Enumerable.Range(0, bounds).ToList();
            var result = Enumerable.Reverse(sequence);
            for (int i = 0; i < cycles; i++)
            {
                sequence.ForEach(producer.Engage);
                result = result.Concat(Enumerable.Reverse(sequence));
            }

            processor.Process(result.GetEnumerator());
            processor.Process(null);
        }

        [Test]
        public void MessageBusChannelTestDrainCycle()
        {
            TestCommutator<TestCall>.Prepare();

            int bounds = UnityEngine.Random.Range(10, 20);
            int cycles = UnityEngine.Random.Range(2, 5);

            var producer = new Producer();
            var processor = new Processor();
            var sequence = Enumerable.Range(0, bounds).ToList();
            for (int i = 0; i < cycles; i++)
            {
                sequence.ForEach(producer.Engage);
                processor.Process(Enumerable.Reverse(sequence).GetEnumerator());
            }
        }

        [Test]
        public void MessageBusChannelTestQueueDrainTest()
        {
            TestCommutator<TestCall>.Prepare();

            int bounds = UnityEngine.Random.Range(10, 20);
            int cycles = UnityEngine.Random.Range(2, 5);

            var producer = new Producer();
            var sequence = Enumerable.Range(0, bounds).ToList();
            for (int i = 0; i < cycles; i++)
            {
                sequence.ForEach(producer.Engage);

                int readId = 0;
                Option<TestCall> call;
                while ((call = TestCommutator<TestCall>.Default.DrainQueue(ref readId)).HasValue)
                {
                    Assert.IsTrue(sequence.InBounds(readId - 1));
                    Assert.AreEqual(sequence[readId - 1], call.Value.Id);
                }
            }
        }

        [Test]
        public void MessageBusChannelTestDrainQueueRef()
        {
            TestCommutator<TestCall>.Prepare();

            int bounds = UnityEngine.Random.Range(10, 20);
            int cycles = UnityEngine.Random.Range(2, 5);

            var producer = new Producer();
            var sequence = Enumerable.Range(0, bounds).ToList();

            for (int i = 0; i < cycles; i++)
            {
                sequence.ForEach(producer.Engage);

                int readId = 0;
                while (TestCommutator<TestCall>.Default.CanDrainQueue(readId))
                {
                    ref readonly var call = ref TestCommutator<TestCall>.Default.DrainQueueRef(ref readId);
                    Assert.IsTrue(sequence.InBounds(readId - 1));
                    Assert.AreEqual(sequence[readId - 1], call.Id);
                }
                TestCommutator<TestCall>.Default.ResetCounter();
            }
        }
    }
}
