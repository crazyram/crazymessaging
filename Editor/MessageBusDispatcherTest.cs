﻿using CrazyRam.Core.MessageBus;
using NUnit.Framework;

namespace CrazyRam.Editor
{
    public class MessageBusDispatcherTest
    {
        public struct TestCall
        {
            public readonly int Id;

            public TestCall(int id)
            {
                Id = id;
            }
        }

        private class Producer
        {
            public void Engage(int id)
            {
                DispatcherTest<TestCall>.Default.Raise(new TestCall(id));
            }
        }

        private static class DispatcherTest<T>
        {
            public static Dispatcher<T> Default = Dispatcher<T>.New();
        }

        private class Receiver : IMessageReceiver<TestCall>
        {
            public int Id;

            public Receiver()
            {
                DispatcherTest<TestCall>.Default += this;
            }

            ~Receiver()
            {
                DispatcherTest<TestCall>.Default -= this;
            }

            public void OnMessage(in TestCall data)
            {
                Id = data.Id;
            }
        }

        [Test]
        public void MessageBusDispatcherTestSimplePasses()
        {
            var producer = new Producer();
            var receiver = new Receiver();

            producer.Engage(42);
            Assert.AreEqual(receiver.Id, 42);

            producer.Engage(11);
            Assert.AreEqual(receiver.Id, 11);

            DispatcherTest<TestCall>.Default.Clear();

            producer.Engage(42);
            Assert.AreEqual(receiver.Id, 11);
        }
    }
}
